package fugit

import (
	"fmt"
	"time"
)

// Scheduler is the top-level controller of all jobs to be run by fugit
// new schedulers should be created via NewScheduler()
type Scheduler struct {
	ticker   *time.Ticker
	jobs     []*Job
	stopChan chan bool
	getNow   func() time.Time
}

// NewScheduler creates a new Scheduler ready to have jobs added to it
func NewScheduler(nowFunc func() time.Time) *Scheduler {
	s := &Scheduler{
		time.NewTicker(time.Second),
		make([]*Job, 0),
		make(chan bool, 1),
		nowFunc,
	}
	return s
}

// Run begins the time loop and evaluates if added jobs should be run
func (s *Scheduler) Run() {
	defer s.ticker.Stop()
	for {
		select {
		case <-s.stopChan:
			fmt.Println("stopping")
			return
		case t := <-s.ticker.C:
			//fmt.Println("tick ", t)
			for _, j := range s.jobs {
				if j.DueBy(t) {
					j.Run()
				}
			}
		}
	}
}

// Stop cleanly stops a running Scheduler
func (s *Scheduler) Stop() {
	s.stopChan <- true
}

// EveryN sets the frequency of a schedule
//  s.EveryN(3).Seconds().Do(...)
// Will add a new job to the Scheduler s that runs every 3 seconds
func (s *Scheduler) EveryN(frequency uint) *Job {
	j := &Job{ID: 1, frequency: frequency, getNow: s.getNow}
	s.jobs = append(s.jobs, j)
	return j
}

// Every is shorthand for EveryN(1)
func (s *Scheduler) Every() *Job {
	return s.EveryN(1)
}
