# Fugit

*Ticking away... The moments that make up a dull day*

![logo](img/logo.png)

Fugit was designed to be used by a chat bot. This chat bot being designed to run for days on end, needed a simple way to run functions on a schedule. Cron was overkill and would rely on some form of IPC, so Fugit was born. If you have a long running go server/daemon that needs to run functions on a schedule, Fugit will make that easy for you.

## Features 

- [x] run a schedule
- [x] create tasks that run every...
  - [x] second
  - [x] minute
  - [x] hour
  - [x] day
  - [x] weekday
  - [x] day of week
- [x] define a time of day to run tasks 
- [ ] define tasks with arguments
- [x] be able to fake time.Ticker output (for testing)

## Install

To use in your project a simple run the following in your shell with $GOPATH set...

`go get -u gitlab.com/incytestudios/fugit`


## Use
Then in your project source do something like the following example...

```go
import "gitlab.com/incytestudios/fugit"

func MyCoolFunction() {
  ...
}

func main() {
  s := fugit.NewScheduler()
  s.Every().Day().At("14:00").Do(MyCoolFunction)
  s.Run()
}
```

## Design

Fugit provides a Scheduler type that holds Job instances. Each job holds a callable reference as well as timing information as to when it should be ran. While running the Scheduler loops over jobs and runs the ones that ought to run. Jobs are executed in goroutines so the entire scheduler will not block. Take care to ensure your jobs do not need to access the same memory at the same time without using some form of guard (mutex, semaphore, channel).
