package fugit

import (
	"fmt"
	"testing"
	"time"
	// "github.com/davecgh/go-spew/spew"
)

func sampleFunc() {
	fmt.Println("sample func running")
}

func getNow(day time.Weekday) func() time.Time {
	return func() time.Time {
		baseday := 24 // Sunday
		baseday += int(day)
		return time.Date(2019, 3, baseday, 12, 0, 0, 0, time.UTC)
	}
}

func TestRelative(t *testing.T) {
	// used for comparisons
	now := time.Now()
	todayNoon := time.Date(now.Year(), now.Month(), now.Day(), 12, 0, 0, 0, now.Location())
	tomorrowNoon := todayNoon.AddDate(0, 0, 1)

	// schedule an every day job with default time
	s := NewScheduler(time.Now)
	weekdayJob := s.Every().Day().Do(sampleFunc)
	targetDue := todayNoon

	if now.After(todayNoon) {
		targetDue = tomorrowNoon
	}

	if !weekdayJob.NextDue().Equal(targetDue) {
		t.Errorf("due date:%s doesn't match expected date:%s", weekdayJob.NextDue(), targetDue)
	}
}

func TestWeekDay(t *testing.T) {
	// schedule an every day job with default time
	s := NewScheduler(getNow(time.Saturday))
	weekdayJob := s.Every().Weekday().Do(sampleFunc)

	if weekdayJob.NextDue().Weekday() == time.Saturday || weekdayJob.NextDue().Weekday() == time.Sunday {
		t.Error("due date should not run on Saturday or Sunday")
	}

	if weekdayJob.NextDue().Weekday() != time.Monday {
		t.Error("due date should be Monday")
	}
}

func TestWeekAfterAt(t *testing.T) {
	// schedule an every day job with default time
	s := NewScheduler(getNow(time.Saturday))
	weekdayJob := s.Every().Weekday().At("12:01").Do(sampleFunc)

	if weekdayJob.NextDue().Weekday() == time.Saturday || weekdayJob.NextDue().Weekday() == time.Sunday {
		t.Error("due date should not run on Saturday or Sunday")
	}

	if weekdayJob.NextDue().Weekday() != time.Monday {
		t.Error("due date should be Monday")
	}
}

func TestMonday(t *testing.T) {
	// schedule an every day job with default time
	s := NewScheduler(getNow(time.Monday))
	weekdayJob := s.Every().Weekday().Do(sampleFunc)

	if weekdayJob.NextDue().Weekday() == time.Saturday || weekdayJob.NextDue().Weekday() == time.Sunday {
		t.Error("due date should not run on Saturday or Sunday")
	}

	if weekdayJob.NextDue().Weekday() != time.Monday {
		t.Error("due date should be Monday")
	}
}

func TestAtDateLaterNoon(t *testing.T) {
	// schedule an every day job with default time
	s := NewScheduler(getNow(time.Tuesday))
	weekdayJob := s.Every().Weekday().At("12:01").Do(sampleFunc)

	if weekdayJob.NextDue().Weekday() == time.Saturday || weekdayJob.NextDue().Weekday() == time.Sunday {
		t.Error("due date should not run on Saturday or Sunday")
	}

	if weekdayJob.NextDue().Weekday() != time.Tuesday {
		t.Error("due date should be Tuesday")
	}
}

func TestAtDateBeforeNoon(t *testing.T) {
	// schedule an every day job with default time
	s := NewScheduler(getNow(time.Tuesday))
	weekdayJob := s.Every().Weekday().At("11:01").Do(sampleFunc)

	if weekdayJob.NextDue().Weekday() == time.Saturday || weekdayJob.NextDue().Weekday() == time.Sunday {
		t.Error("due date should not run on Saturday or Sunday")
	}

	if weekdayJob.NextDue().Weekday() != time.Wednesday {
		t.Error("due date should be Wednesday")
	}
}

func TestAtDateBeforeNoonEveryDay(t *testing.T) {
	// schedule an every day job with default time
	s := NewScheduler(getNow(time.Tuesday))
	weekdayJob := s.Every().Day().At("11:01").Do(sampleFunc)

	if weekdayJob.NextDue().Weekday() != time.Wednesday {
		t.Error("due date should be Wednesday")
	}
}

func TestBasicRun(t *testing.T) {
	count := 0
	updateCount := func() {
		count++
	}
	s := NewScheduler(time.Now)
	s.EveryN(2).Seconds().Do(updateCount)
	go s.Run()
	done := time.Tick(4 * time.Second)
	<-done
	if count != 1 {
		t.Errorf("count should be 1 but was %d", count)
	}
}
