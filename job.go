package fugit

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

// Job represents a callable function to be run at some future time along with a
// schedule for how often to schedule the work
type Job struct {
	ID        int
	unit      timeUnit
	frequency uint
	useAt     bool
	atHour    uint8
	atMinute  uint8
	due       time.Time
	callable  func()
	getNow    func() time.Time
}

// DueBy returns returns true when this job should be run
func (j *Job) DueBy(t time.Time) bool {
	return t.Equal(j.due) || t.After(j.due)
}

// NextDue returns the next time the job should be run
func (j *Job) NextDue() time.Time {
	return j.due
}

// Run runs the callable function in a gorouting and schedules the next run
func (j *Job) Run() {
	go j.callable()
	j.updateDue()
}

// Seconds sets this job's period to the order of seconds
func (j *Job) Seconds() *Job {
	j.unit = second
	return j
}

// Minutes sets this job's period to the order of minutes
func (j *Job) Minutes() *Job {
	j.unit = minute
	return j
}

// Day sets this job's period to the order of days
// If At() is not called after this, it will run at noon server time
func (j *Job) Day() *Job {
	j.unit = day
	return j
}

// Weekday works like Day but doesn't run Saturday or Sunday
func (j *Job) Weekday() *Job {
	j.unit = weekday
	return j
}

// At allows defining a time of day the task should run when set to a higher
// order than hour. The format is "HH:MM"
//  s.Every().Day().At("15:31").Do(...)
// The above will set a task to run at 3:31pm every day
func (j *Job) At(at string) *Job {
	at = strings.TrimSpace(at)
	chunks := strings.Split(at, ":")
	if len(chunks) == 2 {
		if h, err := strconv.Atoi(chunks[0]); err == nil && h >= 0 && h <= 23 {
			j.atHour = uint8(h)
		} else {
			panic(fmt.Sprintf("bad hour in at string: %s", chunks[0]))
		}
		if m, err := strconv.Atoi(chunks[1]); err == nil && m >= 0 && m <= 59 {
			j.atMinute = uint8(m)
		} else {
			panic(fmt.Sprintf("bad minute in at string: %s", chunks[1]))
		}
		j.useAt = true
	}
	// fmt.Println("parsing AT string", at)
	return j
}

// Do defines the work a Job will do when scheduled time comes
func (j *Job) Do(callable func()) *Job {
	j.callable = callable
	j.updateDue()
	return j
}

// updateDue is how a job determines the next time it should run
// TODO: break this up
func (j *Job) updateDue() {
	period := time.Duration(j.frequency)
	now := j.getNow()
	switch j.unit {
	case invalid:
		panic("invalid time unit")
	case second:
		period *= time.Second
	case minute:
		period *= time.Minute
	case hour:
		period *= time.Hour
	case day, weekday:
		period *= time.Hour * 24
		if !j.useAt { // they didn't say at what time, assume noon
			j.useAt = true
			j.atHour = 12
			j.atMinute = 0
		}
	case week, monday, tuesday, wednesday, thursday, friday, saturday, sunday:
		period *= time.Hour * 24 * 7
		if !j.useAt { // they didn't say at what time, assume noon
			j.useAt = true
			j.atHour = 12
			j.atMinute = 0
		}
	}

	j.due = now.Add(period)

	if j.useAt {
		timeToday := time.Date(now.Year(), now.Month(), now.Day(), int(j.atHour),
			int(j.atMinute), 0, 0, now.Location())
		j.due = timeToday

		if now.After(timeToday) {
			j.due = timeToday.AddDate(0, 0, 1)
		}

		if j.unit == weekday {
			switch j.due.Weekday() {
			case time.Saturday:
				j.due = j.due.AddDate(0, 0, 2)
			case time.Sunday:
				j.due = j.due.AddDate(0, 0, 1)
			}
		}
	}
}
