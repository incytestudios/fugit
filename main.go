package fugit

// timeUnit is used by Jobs to calculate their due dates
type timeUnit int

const (
	invalid = iota
	second
	minute
	hour
	day
	// week should be used if want a weekly period and don't care which day of the
	// week the execution happens on
	week
	weekday // Monday-Friday only
	sunday
	monday
	tuesday
	wednesday
	thursday
	friday
	saturday
)
